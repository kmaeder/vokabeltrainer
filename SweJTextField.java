import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.text.Keymap;
import javax.swing.KeyStroke;
import javax.swing.JTextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

/**
 * Stellt auf Basis des JTextFields ein Textfeld zur Verfügung, dass die Eingabe des schwedischen
 * "å" (a mit kleinem Kreis darüber) direkt über die Taste "ü" der deutschen Tastatur ermöglicht.
 * Version 1.0 (17.4.2011)
 *      - Initial
 * Versuch 1.1 (20.5.2011)
 *      - Einfuegen der Buchstaben erfolgt nun an aktueller Caretposition und nicht immer am Ende 
 *      des Textes
 * 
 * @author Kevin Mäder
 * @version 1.1, 20.5.2011
 */
public class SweJTextField extends JTextField{

    public SweJTextField(){
        super();
        
        //Action-Subklasse fuer das kleine schwedische o
        Action schwedischo = new AbstractAction(){
            public void actionPerformed(ActionEvent actionevent){
                JTextField source = (JTextField) actionevent.getSource();
                //aktuelle Position der Eingabemarke holen
                int position = source.getCaretPosition();
                //Text holen
                String text = (source.getText());
                //Einfuegen des gewollten Buchstaben im Text (UTF-8 Code) an aktueller Position
                source.setText(text.substring(0,position)+'\u00E5'+text.substring(position));
                //Caretposition aktualisieren
                source.setCaretPosition(position+1);
            }
        };
        
        //Action-Subklasse fuer das grosse schwedische O
        Action schwedischO = new AbstractAction(){
            public void actionPerformed(ActionEvent actionevent){
                JTextField source = (JTextField) actionevent.getSource();
                //aktuelle Position der Eingabemarke holen
                int position = source.getCaretPosition();
                //Text holen
                String text = (source.getText());
                //Einfuegen des gewollten Buchstaben im Text (UTF-8 Code) an aktueller Position
                source.setText(text.substring(0,position)+'\u00C5'+text.substring(position));
                //Caretposition aktualisieren
                source.setCaretPosition(position+1);
            }
        };
        
        //Tastaturzuordnung fuer grosses und kleines ü holen
        KeyStroke tasteueklein = KeyStroke.getKeyStroke('ü');
        KeyStroke tasteuegross = KeyStroke.getKeyStroke('Ü');
        
        //aktuelle Keymap des TextFields holen
        Keymap keym = this.getKeymap();
        
        //Loeschen der urspruenglichen Tastaturzuordnung fuer kleines ü
        keym.removeKeyStrokeBinding(tasteueklein);
        //Setzen der neuen Tastaturzuordnung
        keym.addActionForKeyStroke(tasteueklein, schwedischo);
        
        //Loeschen der urspruenglichen Tastaturzuordnung fuer grosses Ü
        keym.removeKeyStrokeBinding(tasteuegross);
        //Setzen der neuen Tastaturzuordnung
        keym.addActionForKeyStroke(tasteuegross, schwedischO);
        
        //Setzen der modifizierten Keymap
        this.setKeymap(keym);
    }
}
