import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowListener;
import java.awt.event.WindowEvent;

import javax.swing.JCheckBox;

import java.lang.Math;
/**
 * Steuert das Benutzerinterface(Fenster) des Vokabeltrainers.
 * Version 1.1  (20.5.2011)
 *      - Benutzung des Einstellungsdialogs eingefuegt
 *      - verschiedene Modi (Verbabfrage und Normal) wieder gelöscht, nun nur noch 
 *      ein Modus mit Einstellung ueber Einstellungsdialog
 * 
 * @author Kevin Mäder 
 * @version 1.1,20.5.2011
 */
public class Controller implements ActionListener,WindowListener{

private final int MAX_ANZAHL_FEHLVERSUCHE = 3;

private Model model;
private View view;
private int currentVokIndex[];

//fuer Pause vor Wechsel zu neuer Vokabel noetig: false - Vokabelabfrage
//                                                true  - Halt vor Wechsel zu neuer Vokabel
//für Verbanfragemodus
private int currentZeitform;

private boolean weiter = false;

//Anzahl der Fehlversuche bei der aktuellen Vokabelabfrage
private int fehlversuche = 0;

    private Controller(){
        
        model = new Model();
        view = new View(this);
        view.auswahlDurchgangDialog(model.getDurchgangsKaesten());
    }
    
    /**
     * Controller ist ActionListener der Buttons und des Textfeldes in View
     */
    public void actionPerformed(ActionEvent e){
        //Button "OK" gedrueckt oder in TextField Enter-Taste gedrueckt
        if ((e.getSource().equals(view.button1))|| (e.getSource().equals(view.field) )){
            
            //nach Halt wegen Anzeige der richtigen Loesung oder nach richtiger Eingabe
            //Wechsel zu neuer Vokabel
            if(weiter){
                //Loeschen der Eingabe und von Label 2
                view.clearText();
                view.setLabel2("");
                //neue Vokabel holen
                nextVok();
                //weiter zuruecksetzen
                weiter = false;
            }
            //aus Vokabelabfrage (weiter == false)
            else{
                if(model.checkUebersetzt(view.getText(),currentVokIndex)){
                    view.setLabel2("richtig");
                    fehlversuche = 0;
                    weiter = true;
                }
                else{
                    fehlversuche++;
                    if (fehlversuche == MAX_ANZAHL_FEHLVERSUCHE){
                        view.setLabel2("falsch, richtig wäre: "+model.getUebersetzt(currentVokIndex));
                        fehlversuche = 0;
                        weiter = true;
                    }
                    else{
                        view.setLabel2("falsch");
                    }
                }   
            }
        }
        //"OK"-Button im Auswahlduchgangsfenster gedrueckt
        if (e.getSource().equals(view.button2)){
            view.disposeAuswahlDurchgangDialog();
        }
        
        //"OK"-Button im Themenauswahlfenster gedrueckt
        if (e.getSource().equals(view.button3)){
            JCheckBox[] cbliste = view.getCheckBox();
            model.voklisteSchreiben();
            model.voklisteLeeren();
            for (int i = 0; i<cbliste.length; i++){
                if(cbliste[i].isSelected()){
                    model.voklisteEinlesen(cbliste[i].getText());
                }
            }
            nextVok();
            view.disposeAuswahlThemenDialog();
        }
        //"OK"-Button im Einstellungsfenster gedrueckt
        if (e.getSource().equals(view.button4)){
            view.disposeEinstellungsDialog();
        }
        
        //Beendenitem im Menu ausgewaehlt
        if (e.getSource().equals(view.mitem[0])){
            boolean close = view.schliessDialog();
            if (close){
                programmBeenden();
            }
        }
        //Durchgangsauswahlitem im Menu ausgewaehlt
        if (e.getSource().equals(view.mitem[1])){
            view.auswahlDurchgangDialog(model.getDurchgangsKaesten());
        }
        //Themenauswahlitem im Menu angewaehlt
        if (e.getSource().equals(view.mitem[2])){
            view.auswahlThemenDialog(model.getListe());
        }
        //Einstellungsitem im Menu angewaehlt
        if (e.getSource().equals(view.mitem[3])){
            view.einstellungsDialog(model.getEinstellungen());
        }
    }
    
    //Methoden fuer Windowlistener
    public void windowActivated(WindowEvent e){
    }
    public void windowClosed(WindowEvent e){
    }
    //Wird von view beim Schliessen des Fensters aufgerufen
    public void windowClosing(WindowEvent e){
        if(e.getSource().equals(view.getHauptFrame())){
            boolean close = view.schliessDialog();
            if (close){
                programmBeenden();
            }
        }
    }
    public void windowDeactivated(WindowEvent e){
    }
    public void windowDeiconified(WindowEvent e){
    }
    public void windowIconified(WindowEvent e){
    }
    public void windowOpened(WindowEvent e){
    }
    
    /**
     * Holt naechste Vokabel aus der Datenbank und zeigt sie an
     */
    private void nextVok(){
        currentVokIndex = model.getNextVokNumber();
        if (currentVokIndex[0] >= 0){
            view.setLabel1(model.getDeutsch(currentVokIndex));     
        }
        if (currentVokIndex[0] == -1){
            boolean close = view.fertigDialog();
            if (close){
                programmBeenden();
            }
            else{
                model.vokabellisteZuruecksetzen();
                nextVok();
            }
        }
    }
    
    /**
     * Startet den Vokabeltrainer
     */
    public static void main(String[] args){
        Controller control = new Controller();
        
    }
    
    /**
     * zum Beenden des Pogramms
     */
    private void programmBeenden(){
        view.disposeHauptFrame();
        model.voklisteSchreiben();
        System.exit(0);
    }
}
