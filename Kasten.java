
/**
 * Enthält Informationen über einen Vokabelkasten (Vokabeln mit aehnlicher Anzahl an
 * richtigen Antworten in Folge). Untere und obere Grenze der Anzahl der richtigen
 * Antworten in Folge und ob der Kasten ausgewählt ist, ist gespeichert.
 * Version 1.0 (20.5.2011)
 *      - Intial: untere und obere Grenze fuer Kasten, Aktivierung des Kastens
 * 
 * @author Kevin Mäder 
 * @version 1.0, 20.5.2011
 */
public class Kasten{

    private int untereGrenze;
    //obereGrenze == -1 -> keine obere Grenze
    private int obereGrenze;
    private boolean aktiviert;

    public Kasten(int uG, int oG, boolean aktiv){
        untereGrenze = uG;
        obereGrenze = oG;
        aktiviert = aktiv;
    }
    
/*    public int getUntereGrenze(){
        return untereGrenze;
    }

    public int getObereGrenze(){
        return obereGrenze;
    }*/
    
    public boolean isAktiviert(){
        return aktiviert;
    }

    public boolean isInKasten(int anzahlRichtige){
        if(obereGrenze == -1){
            return (anzahlRichtige >= untereGrenze);
        }
        else{
            return ((anzahlRichtige >= untereGrenze) && (anzahlRichtige <= obereGrenze));
        }
    }
    
    public String getViewString(){
        //String ret = "";
        if (obereGrenze > -1){
            return untereGrenze + "-" + obereGrenze + " Durchgänge";
        }
        else{
            return untereGrenze + "+ Durchgänge";
        }
    }

    /**
     * Aktivierung des Kastens setzen
     */
    public void setAktiviert(boolean aktiv){
        aktiviert = aktiv;
    }
}
