import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.Collections;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.Set;
import java.io.File;

/**
 * Model des Trainers: Verwaltet die Vokabeln in verschiedenen Listen
 * (z.B. vokabelliste) und prueft die Eingaben auf Richtigkeit
 * Version 1.1 (20.5.2011)
 *      - Verbabfrage (Konjugationsformen) von Modelverb mitintegriert -> Modelverb geloescht
 *      - Substantivabfrage integriert
 *      - Durchgangsanzahlkasten verallgemeinert (NEU: Klasse Kasten)
 *      - Einlesen der Vokabelliste nun direkt ueber ein File-Objekt (Listen.txt wird ueberfluessig)
 *      - Einstellungen zum An- und Abschalten von speziellen Wortartenfunktionen(Verbformen, 
 *      Substantivformen) integriert
 * 
 * @author Kevin Mäder 
 * @version 1.1,20.5.2011
 */
public class Model{

    //Unterverzeichnispfad fuer Vokabellisten
    private final String PFAD = "Vokabeln/";
    private final String ENDUNG = ".txt";

    //Liste fuer Vokabeln die noch abgefragt werden koennen
    private ArrayList<Vokabel> vokabelliste;
    //speichert in der aktuellen Sitzung schon gewusste Vokabeln
    private ArrayList<Vokabel> richtigbeantwortet;
    
    private TreeMap<String,Boolean> voklisten;
    
    //Tabelle fuer Anzahldurchgangskaesten (Argumente: untere Grenze, obere Grenze(-1: keine obere Grenze), standardmaessig aktiviert)
    private Kasten[] durchgangsKaesten = { new Kasten(0,4,false) , new Kasten(5,9,false), new Kasten(10,13,false),new Kasten(14,-1,false) };
    
    //Einstellungen (Substantivformen aktiviert, Verbkonjugationen aktiviert, Adjektivformen aktiviert)
    private boolean[] einstellungen = {true, true, true};
    
    

    /**
     * Konstruktor
     * Liest Vokabeln aus der Vokabelliste ein und speichert sie in der
     * Vokabelliste vokabelliste
     */
    public Model(){
        vokabelliste = new ArrayList<Vokabel>();
        richtigbeantwortet = new ArrayList<Vokabel>();
        
        voklisten = new TreeMap<String,Boolean>();
        
        String[] listen = (new File(PFAD)).list();
        for (int i = 0;i<listen.length;i++){
            if(listen[i].endsWith(ENDUNG)){
                voklisten.put(listen[i].substring(0,listen[i].length()-ENDUNG.length()),new Boolean(false));
            }
        }
    }
    
    /**
     * Abfragen der deutschen Entsprechung der Vokabel
     */
    public String getDeutsch(int[] index){
        if (index[0] >= 0){
            String[] de = vokabelliste.get(index[0]).getDeutsch();
            String ret = de[0];
            for (int j = 1; j<de.length;j++){
                ret = ret + "/"+ de[j];
            }
            //bei Verben wird noch eine bestimmte Konjugation auswaehlt
            if ((vokabelliste.get(index[0]) instanceof Verb) && einstellungen[1]){
                ret = ret +" ("+Verb.getForm(index[1])+")";
            }
            //bei Substantiven wird noch eine bestimmte Form auswaehlt
            if ((vokabelliste.get(index[0]) instanceof Substantiv) && einstellungen[0]){
                ret = ret +" ("+Substantiv.getForm(index[1])+")";
            }
            
            //bei Adjektiven wird noch eine bestimmte Deklination auswaehlt
            if ((vokabelliste.get(index[0]) instanceof Adjektiv) && einstellungen[2]){
                ret = ret +" ("+Adjektiv.getForm(index[1])+")";
            }
            
            return ret;
        }
        else{
            return "keine weiteren Vokabeln, nochmals?";
        }
    }
    
    /**
     * Abfragen der uebersetzten Entsprechung der Vokabel
     */
    public String getUebersetzt(int[] index){
        if (index[0] >= 0){
            String[] ue = vokabelliste.get(index[0]).getUebersetzt();
            String ret = ue[0];
            
            //bei Verben wird nur die gewaehlte Konjugationsform uebergeben
            if ((vokabelliste.get(index[0]) instanceof Verb) && einstellungen[1]){
                ret = ue[index[1]];
            }
            else if ((vokabelliste.get(index[0]) instanceof Substantiv) && einstellungen[0]){
                //bei Substantiven wird nur die gewaehlte Form uebergeben
                ret = ue[index[1]];
            }
            else if ((vokabelliste.get(index[0]) instanceof Adjektiv) && einstellungen[2]){
                //bei Adjektiven wird nur die gewaehlte Form uebergeben
                ret = ue[index[1]];
            }
            else{ 
                for (int j = 1; j<ue.length;j++){
                    ret = ret + "/"+ ue[j];
                }
            }
            return ret;
        }
        else{
            return "keine weiteren Vokabeln, nochmals?";
        }
    }
    
    /**
     * Ueberprueft die eingegebene deutsche Entsprechung auf
     * Richtigkeit, Setzt den Zaehler fuer die Anzahl der richtigen
     * Antworten in Folge
     */
    public boolean checkDeutsch(String text,int[] index){
        String[] de = vokabelliste.get(index[0]).getDeutsch();
        for (int j = 0; j< de.length; j++){
             if (de[j].equals(text)){
                 vokabelliste.get(index[0]).setAnzahlRichtig((vokabelliste.get(index[0]).getAnzahlRichtig()+1));
                 richtigbeantwortet.add(vokabelliste.remove(index[0]));
                 return true;
             }
        }
        vokabelliste.get(index[0]).setAnzahlRichtig(0);
        return false;
    }
    
    /**
     * Ueberprueft die eingegebene uebersetzte Entsprechung auf
     * Richtigkeit(nicht casesenstiv), Setzt den Zaehler fuer die Anzahl der richtigen
     * Antworten in Folge
     */
    public boolean checkUebersetzt(String text,int[] index){        
        String[] ue = vokabelliste.get(index[0]).getUebersetzt();
        int uG = 0;
        int oG = ue.length;
        
        //bei Verben ist nur eine bestimmte Konjugationsform richtig, Einengung auf diese
        if ((vokabelliste.get(index[0]) instanceof Verb) && einstellungen[1]){
            uG = index[1];
            oG = index[1]+1;
        }
        //bei Substantiven ist nur eine bestimmte Form richtig, Einengung auf diese
        if ((vokabelliste.get(index[0]) instanceof Substantiv) && einstellungen[0]){
            uG = index[1];
            oG = index[1]+1;
        }
        //bei Adjektiven ist nur eine bestimmte Form richtig, Einengung auf diese
        if ((vokabelliste.get(index[0]) instanceof Adjektiv) && einstellungen[2]){
            uG = index[1];
            oG = index[1]+1;
        }
        
        for (int j = uG; j< oG; j++){
            if (ue[j].toLowerCase().equals(text.toLowerCase())){
                vokabelliste.get(index[0]).setAnzahlRichtig((vokabelliste.get(index[0]).getAnzahlRichtig()+1));
                richtigbeantwortet.add(vokabelliste.remove(index[0]));
                return true;
            }
        }
        vokabelliste.get(index[0]).setAnzahlRichtig(0);
        return false;

    }
    
    public TreeMap<String,Boolean> getListe(){
        return voklisten;
    }
    
    public Kasten[] getDurchgangsKaesten(){
        return durchgangsKaesten;
    }
    
    public boolean[] getEinstellungen(){
        return einstellungen;
    }
    
    /**
     * Sucht naechste Vokabel aus der Liste fuer Abfrage
     * Gibt Array mit Nummer der Vokabel in der Liste zurueck
     * Gibt -1 zurueck, wenn die Vokabelliste leer ist oder keine 
     * passende Vokabel vorhanden
     * bei besonderen Wortarten (z.B. Verb) mit unterschiedlichen Formen
     * wird im Array nach der Nummer der Vokabel die Nummer der Form uebergeben
     */
    public int[] getNextVokNumber(){
        if (vokabelliste.size() > 0){
            int anfang =(int) (vokabelliste.size()*Math.random());
            int i = anfang;
            int j = 0;

            do{    
               if(j == durchgangsKaesten.length){
                   i++;
                   j = 0;
                   if (i>=vokabelliste.size()){
                       i = 0;
                   }
                   if (i == anfang){
                       break;
                   }
               }
               
               Kasten aktuellerKasten = durchgangsKaesten[j];
               int anzahlRichtig = vokabelliste.get(i).getAnzahlRichtig();
               if(aktuellerKasten.isAktiviert() && (aktuellerKasten.isInKasten(anzahlRichtig))){
                   int[] ret = new int[]{i};
                   if (vokabelliste.get(i) instanceof Verb){
                       ret = new int[]{i, getVerbform()};
                   }
                   if (vokabelliste.get(i) instanceof Substantiv){
                       ret = new int[]{i, getSubstantivform()};
                   }
                   if (vokabelliste.get(i) instanceof Adjektiv){
                       ret = new int[]{i, getAdjektivform((Adjektiv) vokabelliste.get(i))};
                   }
                   return ret;
               }
               j++;
            }
            while(j <= durchgangsKaesten.length);
                    
            return new int[]{-1};
            
        }
        else{
            return new int[]{-1};
        }
    }
    
    public int getVerbform(){
        return (int) ((Verb.anzahlFormen()-1)*Math.random()+1);
    }
    
    public int getSubstantivform(){
        return (int) ((Substantiv.anzahlFormen())*Math.random());
    }
    
    public int getAdjektivform(Adjektiv adj){
        return (int) ((adj.anzahlFormen(adj))*Math.random());
    }
    
    /**
     * wieder Vokabeln in Vokabelliste einfuegen, vor erneutem Durchgang und vorm
     * Zurueckschreiben
     */
    public void vokabellisteZuruecksetzen(){
        while(!richtigbeantwortet.isEmpty()){
            vokabelliste.add(richtigbeantwortet.remove(0));
        }
    }
    
    /**
     * Liest die Vokabeln ein und speichert sie als Vokabeln in der Vokabelliste
     */
    protected void voklisteEinlesen(String subliste){
        if(voklisten.containsKey(subliste)){
            voklisten.put(subliste,new Boolean(true));
        
            DateiLeser leser = new DateiLeser(PFAD+subliste+".txt");
            while(leser.nochMehrZeilen()){
                String zeile = leser.gibZeile();
                if (zeile.startsWith("%")){
                    continue;
                }
                
                //Speichert Wortart: 0=normal, 1=Verb, 2= Substantiv, 3=Adjektiv
                //Erkennung ob Vokabel Verb ist (fuehrendes Leerzeichen)
                int wortart = 0;
                if (zeile.startsWith(Verb.getZeichen())){
                    wortart = 1;
                    zeile = zeile.substring(1);
                }
                //Erkennung ob Vokabel Substantiv ist (fuehrende Raute)
                if (zeile.startsWith(Substantiv.getZeichen())){
                    wortart = 2;
                    zeile = zeile.substring(1);
                }
                //Erkennung ob Vokabel Substantiv ist (fuehrende Raute)
                if (zeile.startsWith(Adjektiv.getZeichen())){
                    wortart = 3;
                    zeile = zeile.substring(1);
                }
                //Aufbauen eines Tokenizers zum Trennen der einzelnen Eintraege pro Zeile
                StringTokenizer tokenizer = new StringTokenizer(zeile,"%");
                int spalte = 0;
                ArrayList<String> de = new ArrayList<String>();
                ArrayList<String> ue = new ArrayList<String>();
                int richtige = 0;

                //Trennen der einzelnen Eintraege innerhalb einer Zeile(Spalte 0: deutsch,
                //Spalte 1: uebersetzt, Spalte 3: Anzahl der richtigen Durchgaenge)
                while(tokenizer.hasMoreElements()){
                    String token = tokenizer.nextToken();
                    if(token.equals("/")){
                        spalte++;
                    }
                    else{
                        switch (spalte){
                            case 0: de.add(token);
                                    break;
                            case 1: ue.add(token);
                                    break;
                            case 2: richtige = Integer.parseInt(token);
                                    break;
                        }
                    }
                }
                // Speichern der einzelnen Elemente in verarbeitbaren Strukturen (Arrays
                // statt Lists)
                String[] deutsch = new String[de.size()];
                for (int i = 0; i<deutsch.length;i++){
                    deutsch[i] = de.get(i);
                }
                String[] uebersetzt = new String[ue.size()];
                for (int i = 0; i<uebersetzt.length;i++){
                    uebersetzt[i] = ue.get(i);
                }
                Vokabel vok = null;
                //Erstellen eines der Wortart entsprechenden Vokabelobjekts
                switch (wortart){
                    case 0: vok = new Vokabel(deutsch, uebersetzt,richtige, subliste);
                            break;
                    case 1: vok = new Verb(deutsch, uebersetzt,richtige, subliste);
                            break;
                    case 2: vok = new Substantiv(deutsch, uebersetzt,richtige, subliste);
                            break;
                    case 3: vok = new Adjektiv(deutsch, uebersetzt,richtige, subliste);
                            break;
                }
                //Abspeichern in der Vokabelliste
                vokabelliste.add(vok);
            }
        }
    }
    /**
     * Rueckschreiben der Vokabelliste in die Datei
     */
    public void voklisteSchreiben(){
        //alle Vokabeln in Vokabelliste schreiben und sortieren
        vokabellisteZuruecksetzen();
        Collections.sort(vokabelliste);
        
        //Vokabeln werden listenweise zurueckgeschrieben
        Set<String> set =voklisten.keySet();
        Iterator<String> it = set.iterator();
        while(it.hasNext()){
            String key = it.next();
            
            //wenn Liste aktiviert war (Themenauswahl) zurueckschreiben der Liste
            if (voklisten.get(key)){
                String name = key;
                //Listendatei wird geoeffnet (neu geoeffnet)
                DateiSchreiber schreiber = new DateiSchreiber(PFAD+name+".txt");
                //i mit dem Ende der Vokabelliste initialisieren
                //Vokabelliste wird in umgekehrter Reihenfolge durchgegangen, da so die
                //Behandlung des Indexs i einfacher wird; Vokabelliste ist entsprechend
                //auch andersherum sortiert (siehe compareTo() in Verb)
                int i = vokabelliste.size()-1;
                while(i>=0){
                    if(name.equals(vokabelliste.get(i).getQuelle())){
                        schreiber.vokSchreiben(vokabelliste.remove(i));
                    }
                    i--;
                }
                schreiber.schreibenBeenden();
            }
        }
    }
    
    /**
     * Loescht alle Eintraege aus der Vokabelliste
     */
    public void voklisteLeeren(){
        vokabelliste = new ArrayList<Vokabel>();
        Set<String> set = voklisten.keySet();
        Iterator<String> it = set.iterator();
        while(it.hasNext()){
            String key = it.next();
            voklisten.put(key,new Boolean(false));
        }
    }
}
