
/**
 * Substantivvokabeln mit verschiedenen Formen (unbestimmt/bestimmt, Einazhl/Mehrzahl)
 * Version 1.0 (20.5.2011)
 *      - Initial: spezielle Wortart mit Unterscheidungszeichen und verschiedenen Formen 
 *      (unbestimmt/bestimmt, Einazhl/Mehrzahl) 
 * 
 * @author Kevin Mäder
 * @version 1.0, 20.5.2011
 */
public class Substantiv extends Vokabel{
    
    //fuehrende Raute
    private static final String ZEICHEN = "#";
    //Namen der einzelnen Sunstantivformen
    private static final String[] FORMEN = {"Einzahl, unbestimmt","Einzahl, bestimmt","Mehrzahl, unbestimmt","Mehrzahl, bestimmt"};

    public Substantiv(String[] d, String[] ueb, int richtige, String quel){
        super(d, ueb, richtige, quel);
    }
    public static String getForm(int i){
        return FORMEN[i];
    }
    
    public static int anzahlFormen(){
        return FORMEN.length;
    }
    
        public static String getZeichen(){
        return ZEICHEN;
    }
    
    public String toString(){
        String ret = ZEICHEN;
        for(int i = 0; i<deutsch.length;i++){
            ret = ret + deutsch[i]+"%";
        }
        ret = ret+"/%";
        for(int i = 0; i<uebersetzt.length;i++){
            ret = ret + uebersetzt[i]+"%";
        }
        ret = ret+"/%"+(new Integer(richtigeantworten).toString());
        return ret;
    }
}
