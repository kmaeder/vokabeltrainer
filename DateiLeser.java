import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.IOException;

/** 
 * Diese Klasse erlaubt das Lesen von Textdateien, welche in UTF8 Zeichnkodierung gespeichert wurden.
 * Dies geschieht unabhängig vom Betriebssystem.
 *
 * Version 1.1 (15.12.2012):
 * - FileReader gegen InputStreamReader getauscht, um Defaultcharset "UTF8" einzustellen. 
 * @author Kevin Mäder
 * @version 1.1
 */
public class DateiLeser{

    private BufferedReader	file;
    private boolean		lesenVersucht;
    private String		gelesenerString;

    /**
     * Erzeuge einen DateiLeser. 
     */
    public DateiLeser(String dateiname) {
	try {
	    file = new BufferedReader(new InputStreamReader(new FileInputStream(dateiname), "UTF8"));
	    lesenVersucht = false;
	} 
    catch (IOException e) {
	    lesenVersucht = true;
	}
	gelesenerString = null;
    }
    

    /**
     * Pruefe, ob noch eine Zeile lesbar ist.
     */
    public boolean nochMehrZeilen() {
        if (!lesenVersucht) {
            leseVersuch();
        }
	    return (gelesenerString != null);
    }

    /**
     * Gibt die naechste Zeile der Datei als String zurueck. Sobald das Ende der Datei erreicht ist,
     * wird null zurueckgegeben.
     */
    public String gibZeile() {
        String ergebnis;
        if (!lesenVersucht){ 
            leseVersuch();
        }
        ergebnis = gelesenerString;
        gelesenerString = null;
        lesenVersucht = false;
        return ergebnis;
    }

    private void leseVersuch() {
        try {
            gelesenerString = file.readLine();
            lesenVersucht = true;
        } 
        catch (IOException e) {
            lesenVersucht = true;
            gelesenerString = null;
        }
    }
    
 
}

