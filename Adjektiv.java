
/**
 * Adjektivvokabeln mit verschiedenen Deklinationen(utrum, neutrum, plural)
 * Version 1.0 (26.9.2011)
 *      - Initial: spezielle Wortart mit Unterscheidungszeichen und verschiedenen Deklinationen
 * 
 * @author Kevin Mäder
 * @version 1.0, 26.9.2011
 */
public class Adjektiv extends Vokabel{
    
    //fuehrende Raute
    private static final String ZEICHEN = "+";
    //Namen der einzelnen Sunstantivformen
    private static final String[] FORMEN = {"Einzahl, utrum","Einzahl, neutrum","Mehrzahl"};
    private static final String[] FORMEN2 = {"Einzahl, utrum","Einzahl, neutrum","Mehrzahl","Komparativ","Superlativ"};

    public Adjektiv(String[] d, String[] ueb, int richtige, String quel){
        super(d, ueb, richtige, quel);
    }
    public static String getForm(int i){
        return FORMEN2[i];
    }
    
    public int anzahlFormen(Adjektiv adj){
        if (adj.uebersetzt.length < FORMEN2.length){
            return FORMEN.length;
        }
        else{
            return FORMEN2.length;
        }
    }
    
        public static String getZeichen(){
        return ZEICHEN;
    }
    
    public String toString(){
        String ret = ZEICHEN;
        for(int i = 0; i<deutsch.length;i++){
            ret = ret + deutsch[i]+"%";
        }
        ret = ret+"/%";
        for(int i = 0; i<uebersetzt.length;i++){
            ret = ret + uebersetzt[i]+"%";
        }
        ret = ret+"/%"+(new Integer(richtigeantworten).toString());
        return ret;
    }
}
