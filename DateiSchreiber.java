import java.io.BufferedWriter;
import java.io.Writer;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
//import java.io.File;
//import java.io.FileWriter;
import java.io.IOException;
/**
 * Schreibt die Vokabelliste zurueck in die Datei 
 * Version 1.1 (20.5.2011)
 *      - Infozeilen fuer weitere Wortarten(Verb, Substantiv) ergaenzt
 * Version 1.2 (15.12.2012)
 *      - Schreibt Datei in UTF8 unabhängig vom Betriebssystem
 * 
 * @author Kevin Mäder
 * @version 1.2,15.12.2012
 */
public class DateiSchreiber{
    
    private BufferedWriter bw;
    private String linefeed = new String("\n");
    
    public DateiSchreiber(String pfad){
        //File file = new File(pfad);
        try{
            Writer w = new OutputStreamWriter(new FileOutputStream(pfad), "UTF8");
            bw = new BufferedWriter(w);

            //Dateikopf schreiben
            bw.write("%Vokabelliste fuer den Vokabeltrainer");
            bw.write(linefeed);
            bw.write("%");
            bw.write(linefeed);
            bw.write("%1.Spalte: deutsch");
            bw.write(linefeed);
            bw.write("%2.Spalte: schwedisch");
            bw.write(linefeed);
            bw.write("%3.Spalte: Anzahl der richtigen Eingaben in Folge");
            bw.write(linefeed);
            bw.write("%Woerter/Wortgruppen mit % trennen");
            bw.write(linefeed);
            bw.write("%Trennung zwischen deutsch und schwedisch mit %/%");
            bw.write(linefeed);
            bw.write("%Verben mit Konjugationsformen mit einem fuehrenden \""+Verb.getZeichen()+"\" markieren");
            bw.write(linefeed);
            bw.write("%Substantive mit (un)bestimmten Formen mit einem fuehrenden \""+Substantiv.getZeichen()+"\" markieren");
            bw.write(linefeed);
            bw.write("%Adjektive mit (un)bestimmten Formen mit einem fuehrenden \""+Adjektiv.getZeichen()+"\" markieren");
            bw.write(linefeed);
            bw.write("%");
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
    /**
     * Methode zum Schreiben einer einzelnen Vokabel
     */
    public void vokSchreiben(Vokabel vok){
        try{
            bw.write(linefeed);
            bw.write(vok.toString());
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
    
    /**
     * Methode zum Beenden des Schreibvorganges
     * (noetig damit Datei nicht leer bleibt)
     */
    public void schreibenBeenden(){
        try{
            bw.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
    
}
