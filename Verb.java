
/**
 * Beschreiben Sie hier die Klasse Verb.
 * Version 1.1 (20.5.2011)
 *      - fuehrende Unterscheidungszeichen eingefuehrt, damit auch toString() angepasst
 *      - Formennamen von Model nach Verb verschoben
 * 
 * @author Kevin Mäder 
 * @version 1.1, 20.5.2011
 */
public class Verb extends Vokabel{

    //fuehrendes Leerzeichen
    private static final String ZEICHEN = " ";
    //Namen der einzelnen Verbkonjugationsformen
    private static final String[] FORMEN = {"infintiv","presens","preteritum","supinum"};

    /**
     * Konstruktor für Objekte der Klasse Verb
     */
    public Verb(String[] d, String[] ueb, int richtige, String quel){
        super(d, ueb, richtige, quel);
    }
    
    public static String getForm(int i){
        return FORMEN[i];
    }
    
    public static int anzahlFormen(){
        return FORMEN.length;
    }
    
    public static String getZeichen(){
        return ZEICHEN;
    }
    
    public String toString(){
        String ret = ZEICHEN;
        for(int i = 0; i<deutsch.length;i++){
            ret = ret + deutsch[i]+"%";
        }
        ret = ret+"/%";
        for(int i = 0; i<uebersetzt.length;i++){
            ret = ret + uebersetzt[i]+"%";
        }
        ret = ret+"/%"+(new Integer(richtigeantworten).toString());
        return ret;
    }
    
}
