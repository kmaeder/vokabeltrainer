schnell:
- Beim Start testen, ob Unterordner "Vokabeln" vorhanden ist und sinnvolle Fehlermeldung ausgeben.
- Anzeige des Lernfortschritts im GUI

mittel:
- beim Ende der Vokabelabfrage bei Frage ob Programm beenden Option "Nein" nutzbar machen

- Fehler: wenn unterster Kasten nicht ausgewählt ist, werden falsche Vokabeln nicht wiederholt
- interne Speicherstruktur der Vokabeln ändern, damit Abfrage "zufälliger" wird
    neue Struktur über TreeModel: Root -> Anzahl Richtige -> Vokabel
    Bsp.: Root -> 0 -> sehen;se
                    -> tanzen;dansa
               -> 2 -> Baum;träd
    einzelne Anzahlen Richtige haben festgelegte Wahrscheinlichkeit für Abfrage
    richtig beantwortete Vokabeln werden aus dem Baum entfernt, falsch beantwortete in "0" verschoben
    (Array mit zuletzt abgefragten Vokabeln (z.B. die 5 letzten voks) damit falsche Vokabeln nicht gleich danach noch einmal gefragt werden)
    alte vokliste bleibt zum Zurückschreiben/Speichern aller Vokabeln erhalten, TreeModel wird für jeden Vokabellernvorgang neu aufgebaut 
    -> damit auch Abspeichern der aktuellen Sitzung möglich

- neue Funktion: Lernfortschritt zurücksetzen
- bei Notationsfehler in Vokabellisten Fehler ausgeben und nicht abstürzen

langfristig:
- neue Funktion: Sessions, Sitzung speichern und später wieder fortsetzen

eventuell:
- neue Funktion: Eingabe von neuen Vokabeln über GUI
- mobile Variante: Vokabeln müssen nicht vollständig eingegeben werden sondern es wird nur abgefragt, ob Vokabel gewusst wurde
