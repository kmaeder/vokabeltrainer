import java.lang.Comparable;
/**
 * Repraesentiert eine Vokabel des Vokabeltrainers
 * Version 1.1 (20.5.2011)
 *      - compareTo-Methode angepasst Sortierung der Woerter (vor
 *      allem der neuen Wortarten Verb und Substantiv) alphabetisch
 *      bleibt + Ignorieren von Gross- und Kleinschreibung beim Sortieren
 * 
 * @author Kevin Mäder 
 * @version 1.1,20.5.2011
 */
public class Vokabel implements Comparable{

    protected String[] deutsch;
    protected String[] uebersetzt;
    protected int richtigeantworten;
    //Speichert Namen der Vokabelliste, von wo die Vokabel eingelesen wurde
    protected String quelle;
    
    public Vokabel(String[] d, String[] ue, int richtige, String quel){
         deutsch = d;
         uebersetzt = ue;
         richtigeantworten = richtige;
         quelle = quel;
    }
    
    public String[] getDeutsch(){
        return deutsch;
    }
    
    public String[] getUebersetzt(){
        return uebersetzt;
    }

    public int getAnzahlRichtig(){
        return richtigeantworten;
    }
    
    public void setAnzahlRichtig(int i){
        richtigeantworten = i;
    }
    
    public String getQuelle(){
        return quelle;
    }
    
    /**
     * Vergleicht Vokabeln miteinander (zum Sortieren)
     */
    public int compareTo(Object vok){
        String eins = this.toString();
        String zwei = vok.toString();
        
        //bei besonderen Vokabel (Verb, Substantiv) wird von toString() ein Unterscheidungs-
        //zeichen vornangestellt. Dieses muss zum alphabetischen Sortieren wieder entfernt
        //werden
        if((this instanceof Verb) || (this instanceof Substantiv) || (this instanceof Adjektiv)){
            eins = eins.substring(1);
        }
        if((vok instanceof Verb) || (vok instanceof Substantiv) || (vok instanceof Adjektiv)){
            zwei = zwei.substring(1);
        }
        return zwei.toLowerCase().compareTo(eins.toLowerCase());
    }
    
    /**
     * Bringt alle Informationen zu der Vokabel in einen String (zum Rueckspeichern
     * und Sortieren)
     */
    public String toString(){
        String ret = "";
        for(int i = 0; i<deutsch.length;i++){
            ret = ret + deutsch[i]+"%";
        }
        ret = ret+"/%";
        for(int i = 0; i<uebersetzt.length;i++){
            ret = ret + uebersetzt[i]+"%";
        }
        ret = ret+"/%"+(new Integer(richtigeantworten).toString());
        return ret;
    }
}
