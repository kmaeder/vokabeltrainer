import java.awt.GridLayout;
import java.awt.Container;

import java.awt.event.ActionEvent;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.Set;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;

/**
 * Klasse fuer grafische Oberflaeche des Vokabeltrainers.
 * Version 1.2 (20.5.2011)
 *      - Einstellungsdialog hinzugefuegt
 *      - Uebergabe und JCheckBox-System bei Anzahldurchgangsauswahl ueberarbeitet
 *      dadurch allgemeine Kastendefinitionen moeglich
 * Version 1.21 (06.6.2011)
 *      - auf der Themenauswahlseite wurde eine CheckBox zum Aktivieren von allen Themen
 *      eingebaut
 * 
 * @author Kevin Mäder
 * @version 1.21,06.6.2011
 */
public class View{

    private JFrame hauptframe;
    protected SweJTextField field;
    private JLabel label1;
    private JLabel label2;
    protected JButton button1;
    protected JMenuItem[] mitem;
    
    private JDialog auswahldurchgangdialog;
    private Kasten[] durchgangsKaesten;
    private JCheckBox[] durchgangCBoxListe;
    protected JButton button2;
    private Controller control;
    
    private JDialog auswahlthemendialog;
    private JCheckBox alleAktivieren;
    protected JCheckBox[] themenCBoxListe;
    protected JButton button3;
    
    private JDialog einstellungdialog;
    private boolean[] einstellungen;
    private JCheckBox[] einstellungCBoxListe;
    protected JButton button4;

    /**
     * Konstruktor, wird durch Controller aufgerufen. Erzeugt Fenster und fuegt
     * Listener hinzu.
     */
    public View(Controller controller){
        
        control = controller;
    
        //Fenster erstellen
        hauptframe = new JFrame("Vokabeltrainer");
        //Controller ist Windowlistener
        hauptframe.addWindowListener(controller);
        
        Container contentPane = hauptframe.getContentPane();
        
        //Layout setzen
        contentPane.setLayout(new GridLayout(4, 1));
        
        //Erstellen der Label, Buttons und TextFields + hinzufügen des Listeners(Controller)
        label1 = new JLabel("");
        label2 = new JLabel("");
        field = new SweJTextField();
        field.addActionListener(controller);
        button1 = new JButton("OK");
        button1.addActionListener(controller);
        JMenuBar menubar = new JMenuBar();
        JMenu menu = new JMenu("Datei");
        
        mitem = new JMenuItem[4];
        mitem[0] = new JMenuItem("Beenden");
        mitem[0].addActionListener(control);
        menu.add(mitem[0]);
        
        menubar.add(menu);
        
        menu = new JMenu("Optionen");
        
        mitem[1] = new JMenuItem("Durchgänge");
        mitem[1].addActionListener(control);
        menu.add(mitem[1]);
        
        mitem[2] = new JMenuItem("Themen");
        mitem[2].addActionListener(control);
        menu.add(mitem[2]);
        
        mitem[3] = new JMenuItem("Einstellungen");
        mitem[3].addActionListener(control);
        menu.add(mitem[3]);
        
        menubar.add(menu);

        //Komponenten auf Fenster einfuegen
        contentPane.add(label1);
        contentPane.add(field);
        contentPane.add(label2);
        contentPane.add(button1);
        
        hauptframe.setJMenuBar(menubar);

        //Fenstergroesse setzen + sichtbar machen
        hauptframe.setSize(400, 150);
        hauptframe.setLocation(200,200);
        
        //Schliessen geht nach OptionPane-Abfrage ueber Funktion dispose -> defaultclose aus
        hauptframe.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        
        hauptframe.setVisible(true);
        
    }
    
    /**
     * Text aus TextField lesen
     */
    protected String getText(){
        return field.getText();
    }
    
    /**
     * Text im Textfield loeschen
     */
    protected void clearText(){
        field.setText("");
    }    
    
    /**
     * Text von Label 1 setzen
     */
    protected void setLabel1(String text){
        label1.setText(text);
    }
    
    /**
     * Text von Label 2 setzen
     */
    protected void setLabel2(String text){
        label2.setText(text);
    }
    
    /**
     * Fenster schliessen
     */
    protected void disposeHauptFrame(){
        hauptframe.dispose();
    }
    
    public JFrame getHauptFrame(){
        return hauptframe;
    }
    
    /**
     * Erstellt das Fenster zur Auswahl der Vokabeln nach der Anzahl der erfolgreichen Durchgaenge
     */
    public void auswahlDurchgangDialog(Kasten[] durchgaenge){
        durchgangsKaesten = durchgaenge;
        auswahldurchgangdialog = new JDialog(hauptframe ,"Auswahl der Vokabeln" , true);
        Container contentPane = auswahldurchgangdialog.getContentPane();
        //Layout setzen
        contentPane.setLayout(new GridLayout(2+durchgangsKaesten.length, 1));
        
        JLabel label3 = new JLabel("Vokabeln mit wievielen erfolgreichen Durchgängen sollen abgefragt werden?");
        contentPane.add(label3);
        
        //CheckBoxes mit den verschiedenen Auswahlmöglichkeiten erstellen und in das Fenster einbinden
        durchgangCBoxListe = new JCheckBox[durchgangsKaesten.length];
        for (int i = 0; i< durchgangCBoxListe.length; i ++){
            durchgangCBoxListe[i] = new JCheckBox(durchgangsKaesten[i].getViewString());
            contentPane.add(durchgangCBoxListe[i]);
        }

        button2 = new JButton("OK");
        button2.addActionListener(control);
        contentPane.add(button2);
        
        auswahldurchgangdialog.setSize(500, (100+18*durchgangCBoxListe.length));
        auswahldurchgangdialog.setLocation(200,200);
        
        //Schliessen geht nur nach Bestaetigung ueber OK
        auswahldurchgangdialog.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        auswahldurchgangdialog.setVisible(true);
        
    }
    
    /**
     * Schliesst Fenster zur Auswahl der Vokabeln nach der Anzahl der erfolgreichen Durchgaenge und speichert die gewählte Auswahl zurück
     */
    public void disposeAuswahlDurchgangDialog(){
        
        //Speichern der Auswahl in den einzelnen Kaesten
        for (int i = 0;i< durchgangsKaesten.length; i++){
            durchgangsKaesten[i].setAktiviert(durchgangCBoxListe[i].isSelected());
        }
        auswahldurchgangdialog.dispose();
    }
    
    /**
     * Erstellt Fenster zur Auswahl der Vokabeln nach Themen
     */
    public void auswahlThemenDialog(TreeMap<String,Boolean> map){
        auswahlthemendialog = new JDialog(hauptframe ,"Themenauswahl", true);
        Container contentPane = auswahlthemendialog.getContentPane();
        //Layout setzen
        contentPane.setLayout(new GridLayout((4+map.size()), 1));
        JLabel label4 = new JLabel("Vokabeln aus welchen Themenbereichen sollen abgefragt werden?");
        contentPane.add(label4);
        
        //Subklasse für das "alle auswählen"-Feld
        Action alle = new AbstractAction(){
            public void actionPerformed(ActionEvent actionevent){
                boolean b = false;
                if(((JCheckBox)actionevent.getSource()).isSelected()){
                    b = true;
                }
                for (int i = 0;i<themenCBoxListe.length;i++){
                    themenCBoxListe[i].setSelected(b);
                }
            }
        };
        
        alleAktivieren = new JCheckBox("alle");
        contentPane.add(alleAktivieren);
        alleAktivieren.addActionListener(alle);
        
        contentPane.add(new JSeparator());
        
        themenCBoxListe = new JCheckBox[map.size()];
        Set<String> set = map.keySet();
        Iterator<String> it = set.iterator();
        int i = 0;
        while(it.hasNext()){
            String key = it.next();
            themenCBoxListe[i] = new JCheckBox(key);
            themenCBoxListe[i].setSelected(map.get(key));
            contentPane.add(themenCBoxListe[i]);
            i++;
        }
        
        button3 = new JButton("OK");
        button3.addActionListener(control);
        contentPane.add(button3);
        
        auswahlthemendialog.setSize(500, (125+18*themenCBoxListe.length));
        auswahlthemendialog.setLocation(200,200);
        
        //Schliessen geht nur nach Bestaetigung ueber OK
        auswahlthemendialog.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        auswahlthemendialog.setVisible(true);
    }
    
    public JCheckBox[] getCheckBox(){
        return themenCBoxListe;
    }
    
    /**
     * Schliesst Fenster zur Auswahl der Vokabeln nach Themen
     */
    public void disposeAuswahlThemenDialog(){
        auswahlthemendialog.dispose();
    }
    
    /**
     * Dialog zum Einstellungen vornehmen oeffnen (Substantivformen und Verbformen an- oder 
     * abschalten
     */
    public void einstellungsDialog(boolean[] einstellung){
        einstellungen = einstellung;
        einstellungdialog = new JDialog(hauptframe ,"Einstellungen", true);
        Container contentPane = einstellungdialog.getContentPane();
        //Layout setzen
        contentPane.setLayout(new GridLayout(4, 1));
        einstellungCBoxListe = new JCheckBox[einstellungen.length];
        einstellungCBoxListe[0] = new JCheckBox("Substantivformen aktivieren",einstellungen[0]);
        einstellungCBoxListe[1] = new JCheckBox("Verbkonjugationen aktivieren",einstellungen[1]);
        einstellungCBoxListe[2] = new JCheckBox("Adjektivformen aktivieren",einstellungen[2]);
        button4 = new JButton("OK");
        button4.addActionListener(control);
        
        contentPane.add(einstellungCBoxListe[0]);
        contentPane.add(einstellungCBoxListe[1]);
        contentPane.add(einstellungCBoxListe[2]);
        contentPane.add(button4);
        
        einstellungdialog.setSize(500,140);
        einstellungdialog.setLocation(200,200);
        
        einstellungdialog.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        einstellungdialog.setVisible(true);
    }
    
    /**
     * Schliesst Einstellungsfenster und speichert Einstellungen zurueck
     */
    public void disposeEinstellungsDialog(){
        for (int i=0;i< einstellungCBoxListe.length;i++){
            einstellungen[i] = einstellungCBoxListe[i].isSelected();
        }
        einstellungdialog.dispose();
    }
    
    /**
     * erzeugt Fenster beim Beenden
     */
    public boolean fertigDialog(){
        int i = JOptionPane.showConfirmDialog(hauptframe,"Keine weiteren Vokabeln. Beenden?","Beenden?",JOptionPane.YES_NO_OPTION);
        if(i == JOptionPane.YES_OPTION){
            return true;
        }
        else{
            return false;
        }
    }
    /**
     * Abfrage beim Versuch das Fenster zu Schliessen, wird von Controller aufgerufen
     */
    protected boolean schliessDialog(){
        int i = JOptionPane.showConfirmDialog(hauptframe,"Möchten Sie das Vokabeltraining beenden?","Beenden?",JOptionPane.YES_NO_OPTION);
        if(i == JOptionPane.YES_OPTION){
            return true;
        }
        else{
            return false;
        }
    }
    
}
